﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task6_SkjelinOttosen
{
    public class Contact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }


        public Contact()
        {

        }

        public Contact(string firstName, string lastname, string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastname;
            PhoneNumber = phoneNumber;
        }

        public Contact(string firstName, string lastname, string phoneNumber, string email)
        {
            FirstName = firstName;
            LastName = lastname;
            PhoneNumber = phoneNumber;
            Email = email;
        }

        public override string ToString()
        {
            string fullName = FirstName + " " + LastName;
            return fullName;
        }
    }
}
