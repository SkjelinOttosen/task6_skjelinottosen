﻿using System;

namespace Task6_SkjelinOttosen
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creates Contacs
            Contact john = new Contact("John", "Mayer", "(800) 477 - 1477", "john@mayer.com");
            Contact sharon = new Contact("Sharon", "Stone", "(700) 439 - 0247", "sharon@stones.com");
            Contact steve = new Contact("Steve", "Jobs", "(100) 100 - 1473", "bill@gates.com");
            Contact bill = new Contact("Bill", "Gates", "(100) 100 - 1473", "steve@jobs.com");
            Contact jim = new Contact("Jim", "Carrey", "(900) 990 - 3332", "jim@carrey.com");

            YellowPage yellowPage2020 = new YellowPage();
            yellowPage2020.AddContacts(john);
            yellowPage2020.AddContacts(sharon);
            yellowPage2020.AddContacts(steve);
            yellowPage2020.AddContacts(bill);
            yellowPage2020.AddContacts(jim);

            while (true)
            {
                Console.WriteLine("Search for a contact: ");
                string searchInput = Console.ReadLine().ToLower();
                yellowPage2020.Search(searchInput);
            }
        }
    }
}
