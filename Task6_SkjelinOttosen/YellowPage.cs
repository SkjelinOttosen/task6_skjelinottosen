﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task6_SkjelinOttosen
{
    class YellowPage
    {
        public List<Contact> Contacts { get; set; }

        public YellowPage()
        {
            Contacts = new List<Contact>();
        }

        public void AddContacts(Contact contact)
        {
            Contacts.Add(contact);
        }

        public bool Search(string searchInput)
        {
            bool isFound = false;

            //Sorts the list
            Contacts.OrderBy(c => c.ToString()).ToList();

            // Checks if the list i empty
            if (Contacts.Count < 1)
            {
                Console.WriteLine("The contact list is empty, please try again tomorrow!");
                return false;
            }
            else
            {
                //Checks if Search criteri is empty
                if (searchInput != "")
                {
                    foreach (Contact contact in Contacts)
                    {
                        // Searching using the fullname of the contact as value
                        if (contact.ToString().ToLower().Contains(searchInput))
                        {
                            isFound = true;
                            Console.WriteLine("Found: " + contact.ToString());
                        }
                    }
                    // Contact not found
                    if (!isFound)
                    {
                        Console.WriteLine("Search criteria did not match, please try again!");
                        return false;
                    }
                    Console.WriteLine();
                }
            }
            return false;
        }
    }
}
